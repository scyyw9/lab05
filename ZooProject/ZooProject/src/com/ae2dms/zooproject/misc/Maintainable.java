package com.ae2dms.zooproject.misc;

public interface Maintainable {
    public void maintain();
}

